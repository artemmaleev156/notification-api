# Notification Api

Notifications api

## Getting started

To start the project input this command:
docker-compose up --build

#### base url
http://localhost:8000/v1/api

## Routes

##### /clients/
POST create a client

##### request body 
```json
{
    "phone": "str" (required),
    "mobile_operator_code": "str" (required),
    "tag": "str" (required),
    "timezone": "str" (required)
}
```
##### response 
```json
{
    "phone": "str",
    "mobile_operator_code": "str",
    "tag": "str",
    "timezone": "str"
}
```

##### /clients/id/
PUT change client's attributes
##### request body 
```json
{
    "phone": "str" (required, format: 7XXXXXXXXXX),
    "mobile_operator_code": "str" (required),
    "tag": "str" (required),
    "timezone": "str" (required)
}
```
##### response 
```json
{
    "phone": "str",
    "mobile_operator_code": "str",
    "tag": "str",
    "timezone": "str"
}
```

##### /clients/id/
DELETE delete the client

##### /mailing_list/
GET get list of mailing list

##### /mailing_list/
POST create a mailing list

##### request body 
```json
{
    "start": "str" (required, format: yyyy-mm-ddTHH:MM:SS),
    "text": "str" (required),
    "filter_mobile_operator_code": "str" (optional),
    "tag": "str" (optional),
    "end": "str" (required format: yyyy-mm-ddTHH:MM:SS)
}
```
##### response
```json
{
    "start": "str",
    "text": "str",
    "filter_mobile_operator_code": "str",
    "tag": "str",
    "end": "str"
}
```

##### /mailing_list/id/
GET get the mailing list
##### response
```json
{
    "start": "str",
    "text": "str",
    "filter_mobile_operator_code": "str",
    "tag": "str",
    "end": "str"
}
```

##### /mailing_list/id/
PUT update the mainling list

##### request body 
```json
{
    "start": "str" (required format: yyyy-mm-ddTHH:MM:SS),
    "text": "str" (required),
    "filter_mobile_operator_code": "str" (optional),
    "tag": "str" (optional),
    "end": "str" (required format: yyyy-mm-ddTHH:MM:SS)
}
```
##### response
```json
{
    "start": "str",
    "text": "str",
    "filter_mobile_operator_code": "str",
    "tag": "str",
    "end": "str"
}
```
##### /mailing_list/id/
DELETE delete the mailing list
##### response
```json
[
    {
        "start": "str",
        "text": "str",
        "filter_mobile_operator_code": "str",
        "tag": "str",
        "end": "str"
    }
]
```

### Compleated additional tasks: 3, 5
