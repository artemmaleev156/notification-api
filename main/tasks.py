from notifications.celery import app
from django.utils import timezone
from threading import Thread

from main.utils import SendRequest
from main.models import MailingList, Client

@app.task
def send_notifications(mailing_list_id):
    mailing_list = MailingList.objects.get(id=mailing_list_id)
    filters = dict()
    if mailing_list.filter_mobile_operator_code:
        filters['mobile_operator_code'] = mailing_list.filter_mobile_operator_code
    if mailing_list.filter_tag:
        filters['tag'] = mailing_list.filter_tag
    clients = Client.objects.filter(**filters)
    for client in clients:
        if timezone.now() == mailing_list.end:
            break
        sr = SendRequest(mailing_list, client)
        Thread(target=sr.run).start()