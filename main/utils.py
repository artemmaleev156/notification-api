import requests

from django.conf import settings
from django.db import transaction 

from .models import Message, MailingList, Client


class SendRequest:
    """Класс создания сообщени и отправки на апи"""
    headers = {'Authorization': f'Bearer {settings.JWT_TOKEN}'}
    url = settings.URL

    def __init__(self, mailing_list: MailingList, client: Client) -> None:
        self.mailing_list = mailing_list
        self.client = client
    
    def create_message(self):
        """Создание сообщения"""
        return Message.objects.create(
            status='Отправлено',
            mailing_list=self.mailing_list,
            client=self.client
        )

    def send_request(self, message):
        """отправка на апи"""
        r = requests.post(
            self.url + f'v1/send/{message.id}', 
            data={
                'id': message.id,
                'phone': self.client.phone,
                'text': self.mailing_list.text
            },
            headers=self.headers)
        resp =  r.json()
        print('response is ', resp)
        return resp 

    def run(self):
        try:
            with transaction.atomic():
                message = self.create_message()
                self.send_request(message)
        except Exception as e:
            print('exception is', str(e))