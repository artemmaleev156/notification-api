from django.urls import URLPattern
from rest_framework import routers
from django.urls import path
from . import views

urlpatterns = [
    path('clients/', views.ClientViewSet.as_view({
        'post': 'create'
    })),
    path('clients/<int:pk>/', views.ClientViewSet.as_view({
        'put': 'update',
        'delete': 'destroy'
    })),
    path('mailing_list/', views.MailingListViewSet.as_view({
        'get': 'list',
        'post': 'create'
    })),
    path('mailing_list/<int:pk>/', views.MailingListViewSet.as_view({
        'get': 'retrieve',
        'put': 'update',
        'delete': 'destroy'
    })),
]


