from dataclasses import field
from email import message
from pyexpat import model
from rest_framework import serializers

from .models import Client, MailingList, Message


class MessageSerializer(serializers.ModelSerializer):
    """Класс сериалайзер сообщений"""
    
    class Meta:
        model = Message
        fields = '__all__'


class ClientSerializer(serializers.ModelSerializer):
    """Класс сериалайзер клиента"""
    
    class Meta:
        model = Client
        fields = ('phone', 'mobile_operator_code', 'tag', 'timezone')
        lookup_field = 'pk'


class MailingListSerializer(serializers.ModelSerializer):
    """Класс сериалайзер рассылки"""
    messages = MessageSerializer(
        many=True
    )

    class Meta:
        model = MailingList
        fields = '__all__'


class SimpleMailingListSerializer(serializers.ModelSerializer):

    class Meta:
        model = MailingList
        fields = '__all__'