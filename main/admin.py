from django.contrib import admin

from .models import *


admin.site.register(MailingList)
admin.site.register(Message)
admin.site.register(Client)