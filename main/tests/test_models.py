from django.forms import ValidationError
from django.test import TestCase
from django.utils import timezone
from django.core.exceptions import ValidationError

from ..models import Client, MailingList, Message


class MailingListModelTest(TestCase):
    """Тест модели Рассылки"""

    def setUp(self):
        self.mailing = MailingList(
            start=timezone.now(),
            text='asd',
            end=timezone.now(),
        )


class ClientModelTest(TestCase):
    """Тест модели Клиент"""
    def setUp(self):
        self.client =  Client(
            phone='7gfadfgadfg',
            mobile_operator_code='123',
            tag='onasda'
        )

    def test_phone_letters_instead_of_digits(self):
        with self.assertRaises(ValidationError):
            self.client.full_clean()

    def test_phone_digits_more_then_11(self):
        self.client.phone = '789789456122'

        with self.assertRaises(ValidationError):
            self.client.full_clean()

    def test_phone_starts_with_no_7(self):
        self.client.phone = '88978945612'

        with self.assertRaises(ValidationError):
            self.client.full_clean()

    