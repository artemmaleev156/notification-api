from rest_framework.viewsets import ModelViewSet

from .models import Client, MailingList
from .serializers import ClientSerializer, MailingListSerializer, SimpleMailingListSerializer


class ClientViewSet(ModelViewSet):
    """Cоздание, удаление и вывод клиентов"""
    queryset = Client.objects.all()
    serializer_class = ClientSerializer

    def create(self, *args, **kwargs):
        """Создание клиента"""
        return super().create(*args, **kwargs)
    
    def update(self, *args, **kwargs):
        """Изменение клиента"""
        return super().update(*args, **kwargs)

    def destroy(self, *args, **kwargs):
        """Удаление клиента"""
        return super().destroy(*args, **kwargs)


class MailingListViewSet(ModelViewSet):
    """добавления новой рассылки со всеми её атрибутами,
       получения общей статистики по созданным рассылкам,
       получения детальной статистики отправленных сообщений,
       обновление атрибутов рассылки,
       удаление рассылки"""
    queryset = MailingList.objects.all()
    serializer_class = MailingListSerializer

    def get_serializer_class(self):
        if self.action in ('create', 'update'):
            return SimpleMailingListSerializer
        return super().get_serializer_class()

    def list(self, *args, **kwargs):
        """получения общей статистики по созданным рассылкам"""
        return super().list(*args, **kwargs)

    def create(self, *args, **kwargs):
        """добавления новой рассылки со всеми её атрибутами"""
        return super().create(*args, **kwargs)
    
    def retrieve(self, *args, **kwargs):
        """получения детальной статистики отправленных сообщений"""
        return super().retrieve(*args, **kwargs)

    def update(self, *args, **kwargs):
        """обновление атрибутов рассылки"""
        return super().update(*args, **kwargs)

    def destroy(self, *args, **kwargs):
        """Удаление рассылки"""
        return super().destroy(*args, **kwargs)