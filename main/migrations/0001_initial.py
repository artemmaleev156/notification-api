# Generated by Django 4.0.3 on 2022-03-10 13:12

import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Client',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('phone', models.CharField(max_length=11, validators=[django.core.validators.RegexValidator(code='invalid_phone', message='invalid phone', regex='7[0-9]')])),
                ('mobile_operator_code', models.CharField(max_length=3)),
                ('tag', models.CharField(max_length=100)),
                ('timezone', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='MailingList',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('start', models.DateTimeField()),
                ('text', models.TextField()),
                ('filter_mobile_operator_code', models.CharField(max_length=100)),
                ('filter_tag', models.CharField(max_length=100)),
                ('end', models.DateTimeField()),
            ],
        ),
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sent', models.DateTimeField()),
                ('status', models.CharField(max_length=100)),
                ('client', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='main.client')),
                ('mailing_list', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='messages', to='main.mailinglist')),
            ],
        ),
    ]
