from django.db.models.signals import post_save
from django.utils import timezone
from django.dispatch import receiver

from .models import MailingList, Client
from .tasks import send_notifications


@receiver(post_save, sender=MailingList)
def create_some(sender, instance, created, **kwargs):
    if created:
        if instance.start < timezone.now() < instance.end:
            send_notifications.delay(instance.id)
        elif instance.start > timezone.now():
            send_notifications.apply_async([instance.id], etc=instance.start)