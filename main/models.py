from django.db import models
from django.core import validators
from django.contrib.postgres.fields import ArrayField


class MailingList(models.Model):
    """Модель рассылки"""
    start = models.DateTimeField()
    text = models.TextField()
    filter_mobile_operator_code = models.CharField(max_length=100, null=True, blank=True)
    filter_tag = models.CharField(max_length=100, null=True, blank=True)
    end = models.DateTimeField()

    def __str__(self) -> str:
        return self.text


class Client(models.Model):
    """Модель клиента"""
    phone = models.CharField(
        max_length=11,
        validators=[validators.RegexValidator(
            regex='7[0-9]',
            message='invalid phone',
            code='invalid_phone'
        )])
    mobile_operator_code = models.CharField(max_length=3)
    tag = models.CharField(max_length=100)
    timezone = models.CharField(max_length=100)

    def __str__(self) -> str:
        return self.phone


class Message(models.Model):
    """модель сообщения"""
    sent = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=100)
    mailing_list = models.ForeignKey(MailingList, 
        on_delete=models.CASCADE, related_name='messages')
    client = models.ForeignKey(Client, 
        on_delete=models.CASCADE)

    def __str__(self) -> str:
        return self.status
